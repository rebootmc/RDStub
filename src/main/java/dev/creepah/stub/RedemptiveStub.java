package dev.creepah.stub;

import tech.rayline.core.plugin.RedemptivePlugin;

public final class RedemptiveStub extends RedemptivePlugin {

    @Override
    protected void onModuleEnable() throws Exception {
        getLogger().info("Loading dependency stub for the Redemptive API!");

        registerCommand(new ReloadCommand());
    }
}
