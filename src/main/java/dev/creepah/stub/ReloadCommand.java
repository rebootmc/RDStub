package dev.creepah.stub;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import tech.rayline.core.command.*;

@CommandPermission("stub.rc")
@CommandMeta(description = "Attempt to reload a plugin's config file")
public final class ReloadCommand extends RDCommand {

    public ReloadCommand() {
        super("rc");
    }

    @Override
    protected void handleCommandUnspecific(CommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /rc <plugin>");

        Plugin plugin = Bukkit.getPluginManager().getPlugin(args[0]);
        if (plugin == null) throw new NormalCommandException("A plugin with the name " + args[0] + " could not be found!");

        plugin.reloadConfig();
        sender.sendMessage(ChatColor.GRAY + "Reloaded " + plugin.getName() + "'s configuration file!");
    }
}
